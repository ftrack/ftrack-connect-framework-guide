###########################
ftrack Framework Guide code
###########################

Support code for the ftrack Framework Guide

*************
Documentation
*************

Full related documentation, including setup guides, can be found at
https://docs.google.com/document/d/e/2PACX-1vRb_bVREeuDPZjFd5ZlgqUkihCETJqCU0jmnv9OYBO8whcgsEsdM_8bJ7C9mVcaa-spcqfiXUbyzTvs/pub

*********************
Copyright and license
*********************

Copyright (c) 2014 ftrack

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this work except in compliance with the License. You may obtain a copy of the
License in the LICENSE.txt file, or at:

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

