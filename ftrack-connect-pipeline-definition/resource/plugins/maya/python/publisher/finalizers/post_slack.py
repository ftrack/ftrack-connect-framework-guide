# :coding: utf-8
# :copyright: Copyright (c) 2014-2020 ftrack

import os
from ftrack_connect_pipeline_maya import plugin
import ftrack_api

from slack import WebClient

class FtrackPostPublishMayaPlugin(plugin.PublisherPostFinalizerMayaPlugin):
    plugin_name = 'post_slack'

    def run(self, context_data=None, data=None, options=None):
        print('@@@ FtrackPostPublishMayaPlugin::run({},{},{})'.format(context_data, data, options))

        # {
        #     "data": [
        #         {
        #             "name": "usd",
        #             "result": [
        #                 {
        #                     "name": "output",
        #                     "result": [
        #                         {
        #                             "name": "usd_output",
        #                             "result": [
        #                                 "/var/folders/0t/mpj_nt0n525cgcvqy_w_641w0000gn/T/tmpok_blcfs.usd"
        #                             ],
        #                             "status": true,
        #                             "category": "plugin",
        #                             "type": "output",
        #                             "plugin_type": "publisher.output",
        #                             "method": "run",
        #                             "user_data": {}
        #                         }
        #                     ],
        #                     "status": true,
        #                     "category": "stage",
        #                     "type": "output"
        #                 }
        #             ],
        #             "status": true,
        #             "category": "step",
        #             "type": "component"
        #         },
        #         {
        #             "name": "thumbnail",
        #             "result": [
        #                 {
        #                     "name": "output",
        #                     "result": [
        #                         {
        #                             "name": "thumbnail",
        #                             "result": [
        #                                 "/var/folders/0t/mpj_nt0n525cgcvqy_w_641w0000gn/T/tmpytbyoilx.jpg.0000.jpg"
        #                             ],
        #                             "status": true,
        #                             "category": "plugin",
        #                             "type": "output",
        #                             "plugin_type": "publisher.output",
        #                             "method": "run",
        #                             "user_data": {}
        #                         }
        #                     ],
        #                     "status": true,
        #                     "category": "stage",
        #                     "type": "output"
        #                 }
        #             ],
        #             "status": true,
        #             "category": "step",
        #             "type": "component"
        #         },
        #         {
        #             "name": "reviewable",
        #             "result": [
        #                 {
        #                     "name": "output",
        #                     "result": [
        #                         {
        #                             "name": "reviewable",
        #                             "result": [
        #                                 "/var/folders/0t/mpj_nt0n525cgcvqy_w_641w0000gn/T/tmps6rwcsm_.mov"
        #                             ],
        #                             "status": true,
        #                             "category": "plugin",
        #                             "type": "output",
        #                             "plugin_type": "publisher.output",
        #                             "method": "run",
        #                             "user_data": {}
        #                         }
        #                     ],
        #                     "status": true,
        #                     "category": "stage",
        #                     "type": "output"
        #                 }
        #             ],
        #             "status": true,
        #             "category": "step",
        #             "type": "component"
        #         },
        #         {
        #             "name": "main",
        #             "result": [
        #                 {
        #                     "name": "pre_finalizer",
        #                     "result": [
        #                         {
        #                             "name": "pre_result",
        #                             "result": {},
        #                             "status": true,
        #                             "category": "plugin",
        #                             "type": "pre_finalizer",
        #                             "plugin_type": "publisher.pre_finalizer",
        #                             "method": "run",
        #                             "user_data": {}
        #                         }
        #                     ],
        #                     "status": true,
        #                     "category": "stage",
        #                     "type": "pre_finalizer"
        #                 },
        #                 {
        #                     "name": "finalizer",
        #                     "result": [
        #                         {
        #                             "name": "result_maya",
        #                             "result": {
        #                                 "asset_version_id": "69c9a234-e8ff-4ad8-b05f-2912beda7c30",
        #                                 "asset_id": "6a3baceb-e870-437b-8982-7d5f6078be9f",
        #                                 "component_names": [
        #                                     "usd",
        #                                     "thumbnail",
        #                                     "reviewable"
        #                                 ]
        #                             },
        #                             "status": true,
        #                             "category": "plugin",
        #                             "type": "finalizer",
        #                             "plugin_type": "publisher.finalizer",
        #                             "method": "run",
        #                             "user_data": {}
        #                         }
        #                     ],
        #                     "status": true,
        #                     "category": "stage",
        #                     "type": "finalizer"
        #                 }
        #             ],
        #             "type": "finalizer"
        #         }
        #     ]
        # }

        # Harvest publish data
        reviewable_path = asset_version_id = None
        for component_data in data:
            if component_data['name'] == 'thumbnail':
                for output in component_data['result']:
                    if output['name'] == 'output':
                        reviewable_path = output['result'][0]['result'][0]
            elif component_data['name'] == 'main':
                for step in component_data['result']:
                    if step['name'] == 'finalizer':
                        asset_version_id = step['result'][0]['result']['asset_version_id']

        # Fetch version
        version = self.session.query('AssetVersion where id={}'.format(asset_version_id)).one()

        client = WebClient("xoxp-748383045924-737397864931-2651169155216-8789226b1359938424d6edb46933b5f9")

        response = client.files_upload(
            channels='test', 
            file=reviewable_path, 
            title='|'.join([cl['name'] for cl in version['asset']['parent']['link']]+[version['asset']['name'],'v%03d'%(version['version'])]),
            initial_comment=version['comment'])

        if response.get('ok') is False:
            raise Exception('Slack file upload failed! Details: {}'.format(response))

        return {}

def register(api_object, **kw):
    if not isinstance(api_object, ftrack_api.Session):
        # Exit to avoid registering this plugin again.
        return
    plugin = FtrackPostPublishMayaPlugin(api_object)
    plugin.register()
