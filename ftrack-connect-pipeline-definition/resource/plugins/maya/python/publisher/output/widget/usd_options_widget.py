# :coding: utf-8
# :copyright: Copyright (c) 2014-2021 ftrack

from functools import partial

from ftrack_connect_pipeline_maya import plugin
from ftrack_connect_pipeline_qt.client.widgets.options.dynamic import DynamicWidget

from Qt import QtWidgets
import ftrack_api


class UsdOptionsWidget(DynamicWidget):

    OPTIONS = {
        'usdFileFormat': {
            'type': 'combobox',
            'label': 'usd File format',
            'options':[
                {'label': 'Binary', 'value': 'usdb'},
                {'label': 'ASCII', 'value': 'usda'},
            ],
            'default':'usda'
        }
    }

    def __init__(
        self, parent=None, session=None, data=None, name=None,
        description=None, options=None, context_id=None, asset_type_name=None
    ):

        self.widgets = {}

        super(UsdOptionsWidget, self).__init__(
            parent=parent,
            session=session, data=data, name=name,
            description=description, options=options,
            context_id=context_id, asset_type_name=asset_type_name)

    def build(self):
        '''build function , mostly used to create the widgets.'''
        super(UsdOptionsWidget, self).build()

        for name, option in self.OPTIONS.items():
            default = None

            if option['type'] == 'checkbox':
                widget = QtWidgets.QCheckBox(option['label'])
            elif option['type'] == 'combobox':
                self.layout().addWidget(QtWidgets.QLabel(option['label']))
                widget = QtWidgets.QComboBox()
                for item in option['options']:
                    widget.addItem(item['label'])
            elif option['type'] == 'line':
                self.layout().addWidget(QtWidgets.QLabel(option['label']))
                widget = QtWidgets.QLineEdit()

            self.widgets[name] = widget
            self.layout().addWidget(widget)

    def set_option_result(self, key, value):
        print("@@@ set_option_result('{}','{}')".format(key, value))
        super(UsdOptionsWidget, self).set_option_result(key,value)

    def current_index_changed(self, name, index):
        self.set_option_result(self.OPTIONS[name]['options'][index]['value'], name)

    def post_build(self):
        super(UsdOptionsWidget, self).post_build()

        for name, widget in self.widgets.items():
            option = self.OPTIONS[name]

            if name in self.options:
                default = self.options[name]
            else:
                default_option = option.get('default_option')
                default = None
                if 0 < len(default_option or ''):
                    # Replace with that option's current value
                    default = self.options[default_option]
                if len(default or '') == 0:
                    default = option.get('default')

            update_fn = partial(self.set_option_result, key=name)
            if option['type'] == 'checkbox':
                if default is None:
                    default = False
                widget.setChecked(default)
                widget.stateChanged.connect(update_fn)
            elif self.OPTIONS[name]['type'] == 'combobox':
                if default is None:
                    default = option['options'][0]['value']
                elif default is not None:
                    idx = 0
                    for item in option['options']:
                        if item['value'] == default or item['label'] == default:
                            widget.setCurrentIndex(idx)
                        idx += 1
                update_fn = partial(self.current_index_changed, name)
                widget.currentIndexChanged.connect(update_fn)
            elif option['type'] == 'line':
                if default is None:
                    default = ""
                else:
                    widget.setText(default)
                widget.textChanged.connect(update_fn)
            
            if not name in self.options and not default is None:
                self.set_option_result(default, name) # Make we have it set to default option

class UsdOptionsPluginWidget(plugin.PublisherOutputMayaWidget):
    plugin_name = 'usd_output'
    widget = UsdOptionsWidget

def register(api_object, **kw):
    if not isinstance(api_object, ftrack_api.Session):
        # Exit to avoid registering this plugin again.
        return
    plugin = UsdOptionsPluginWidget(api_object)
    plugin.register()
