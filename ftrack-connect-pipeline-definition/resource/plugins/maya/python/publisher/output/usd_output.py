# :coding: utf-8
# :copyright: Copyright (c) 2014-2021 ftrack
import platform

import tempfile

import maya.cmds as cmds

from ftrack_connect_pipeline_maya import plugin
import ftrack_api



class OutputMayaUsdPlugin(plugin.PublisherOutputMayaPlugin):

    plugin_name = 'usd_output'

    def run(self, context_data=None, data=None, options=None):
        # ensure to load the usd plugin

        if platform.system().lower() == 'windows':
            cmds.loadPlugin('mayaUsdPlugin.mll', qt=1)
        elif platform.system().lower() == 'linux':
            cmds.loadPlugin('mayaUsdPlugin.so', qt=1)
        elif platform.system().lower() == 'darwin':
            cmds.loadPlugin('mayaUsdPlugin.bundle', qt=1)

        component_name = options['component_name']
        new_file_path = tempfile.NamedTemporaryFile(
            delete=False,
            suffix='.usd'
        ).name

        self.logger.debug(
            'Calling output options: data {}. options {}'.format(
                data, options
            )
        )

        collected_objects = []
        for collector in data:
            collected_objects.extend(collector['result'])

        cmds.select(cl=True)
        cmds.select(collected_objects)
        cmds.mayaUSDExport(file=new_file_path,defaultUSDFormat=options['usdFileFormat'])

        return [new_file_path]


def register(api_object, **kw):
    if not isinstance(api_object, ftrack_api.Session):
        # Exit to avoid registering this plugin again.
        return
    ma_plugin = OutputMayaUsdPlugin(api_object)
    ma_plugin.register()
