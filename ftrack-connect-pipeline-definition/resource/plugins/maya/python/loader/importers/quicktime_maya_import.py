# :coding: utf-8
# :copyright: Copyright (c) 2014-2020 ftrack

import maya.cmds as cmds

from ftrack_connect_pipeline_maya import plugin
import ftrack_api


class QuicktimeMayaImportPlugin(plugin.LoaderImporterMayaPlugin):
    plugin_name = 'quicktime_maya_import'

    def run(self, context_data=None, data=None, options=None):
        # ensure to load the alembic plugin
        cmds.loadPlugin('AbcImport.so', qt=1)

        results = {}

        paths_to_import = []
        for collector in data:
            paths_to_import.extend(collector['result'])

        for component_path in paths_to_import:
            self.logger.debug('Importing path {}'.format(component_path))
            
            imagePlane =  cmds.imagePlane( camera="persp", fileName=component_path)
            cmds.setAttr('{}.type'.format(imagePlane[0]), 2)
            cmds.setAttr('{}.useFrameExtension'.format(imagePlane[0]), True)

            self.logger.info('Imported "{}" to {}.'.format(component_path, imagePlane[0]))

            results[component_path] = imagePlane

        return results


def register(api_object, **kw):
    if not isinstance(api_object, ftrack_api.Session):
        # Exit to avoid registering this plugin again.
        return
    plugin = QuicktimeMayaImportPlugin(api_object)
    plugin.register()