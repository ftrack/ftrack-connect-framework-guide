# :coding: utf-8
# :copyright: Copyright (c) 2019 ftrack

import os
import logging
import re
import shutil

from ftrack_connect_pipeline_maya import host as maya_host
from ftrack_connect_pipeline_qt import event
from ftrack_connect_pipeline import constants

import maya.cmds as cmds
import maya.mel as mm
import maya.utils

import ftrack_api

from ftrack_connect_pipeline.configure_logging import configure_logging
extra_handlers = {
    'maya':{
            'class': 'maya.utils.MayaGuiLogHandler',
            'level': 'INFO',
            'formatter': 'file',
        }
}
configure_logging(
    'ftrack_connect_pipeline_maya',
    extra_modules=['ftrack_connect_pipeline', 'ftrack_connect_pipeline_qt'],
    extra_handlers=extra_handlers,
    propagate=False
)


logger = logging.getLogger('ftrack_connect_pipeline_maya')


created_dialogs = dict()

def get_ftrack_menu(menu_name = 'ftrack', submenu_name = 'pipeline'):
    '''Get the current ftrack menu, create it if does not exists.'''
    gMainWindow = mm.eval('$temp1=$gMainWindow')

    if cmds.menu(
            menu_name,
            exists=True,
            parent=gMainWindow,
            label=menu_name
    ):
        menu = menu_name

    else:
        menu = cmds.menu(
            menu_name,
            parent=gMainWindow,
            tearOff=False,
            label=menu_name
        )

    if submenu_name is not None:
        if cmds.menuItem(
                submenu_name,
                exists=True,
                parent=menu,
                label=submenu_name
            ):
                submenu = submenu_name
        else:
            submenu = cmds.menuItem(
                submenu_name,
                subMenu=True,
                label=submenu_name,
                parent=menu
            )

        return submenu
    else:
        return menu

def _open_dialog(dialog_class, event_manager):
    '''Open *dialog_class* and create if not already existing.'''
    dialog_name = dialog_class

    if dialog_name not in created_dialogs:
        ftrack_dialog = dialog_class
        created_dialogs[dialog_name] = ftrack_dialog(
            event_manager
        )

    created_dialogs[dialog_name].show()

def timeline_init():
    '''
    Set the initial framerate with the values set on the shot
    Setup the timeline.
    '''

    # Set FPS

    fps = str(os.getenv('FPS'))

    if not fps is None and 0 < len(fps):

        mapping = {
            '15': 'game',
            '24': 'film',
            '25': 'pal',
            '30': 'ntsc',
            '48': 'show',
            '50': 'palf',
            '60': 'ntscf',
        }

        fps_maya_type = mapping.get(fps, 'pal')
        if not fps_maya_type is None:
            cmds.warning('Setting current unit to {0}'.format(fps))
            cmds.currentUnit(time=fps_maya_type)
        else:
            cmds.warning("Can't translate {} fps to Maya!".format(fps_maya_type))
    else:
        cmds.warning('No fps supplied!')

    # Set animation timeline

    start = os.getenv('FS')
    end = os.getenv('FE')

    if not start is None and 0 < len(start) and not end is None and 0 < len(end):
        start_int = int(float(start))
        end_int = int(float(end))
        cmds.warning('Setting timeline to {0}-{1}'.format(start_int, end_int))
        cmds.playbackOptions(min=start_int, ast=start_int, aet=end_int, max=end_int)

def scene_init(session):
    ''' Load latest scene, or generate new from template. '''
    
    task = session.query('Task where id={}'.format(os.getenv('FTRACK_CONTEXTID'))).one()
    location = session.pick_location()
    scene_load_path = None
    workfile_project_folder = os.path.join(location.get_filesystem_path(task), 'work')
    workfile_folder = os.path.join(workfile_project_folder, 'scenes')
    workfile_prefix = location.structure.get_resource_identifier(task).replace(os.sep, '_')
    copy_template = True
    if os.path.exists(workfile_project_folder):
        if os.path.exists(workfile_folder):
            copy_template = False
            for filename in sorted(os.listdir(workfile_folder)):
                if os.path.splitext(filename)[-1] in ['.mb','.ma'] and \
                    0 == filename.find(workfile_prefix):
                    scene_load_path = filename
        if scene_load_path:
            scene_load_path = os.path.join(workfile_folder, scene_load_path)
    if copy_template:
        # Copy Maya project template
        template_path = os.path.join(
            location.get_filesystem_path(task['project']),
            '_TEMPLATES',
            task['type']['name'].lower(),
            'maya')
        if os.path.exists(template_path):
            logger.info('Copying Maya project template {} to {}'.format(
                template_path, workfile_project_folder))
            if not os.path.exists(os.path.dirname(workfile_project_folder)):
                os.makedirs(os.path.dirname(workfile_project_folder))
            shutil.copytree(template_path, workfile_project_folder)
        else:
            logger.warning('Maya project template not found @ {}!'.format(
                template_path))
    if not scene_load_path:
        # Copy template to v001 scene
        scene_template_path = os.path.join(workfile_folder, '{}_template.mb'.format(
            task['type']['name'].lower()))
        scene_load_path = os.path.join(workfile_folder, '{}_v001.mb'.format(
            workfile_prefix))
        if os.path.exists(scene_template_path):
            logger.info('Copying Maya template scene {} to {}'.format(
                scene_template_path, scene_load_path))
            shutil.copy(scene_template_path, scene_load_path)
        else:
            logger.warning('Maya scene template not found @ {}!'.format(
                scene_template_path))

    # Load the scene
    logger.info('Loading: {}'.format(scene_load_path))
    cmds.file(scene_load_path, open=True)


def set_in_progress(session):
    ''' Set the launched task in progress '''
    task = session.query('Task where id={}'.format(os.getenv('FTRACK_CONTEXTID'))).one()
    print('Setting launched task "{}"" in progress, was: {}!'.format(task['name'], task['status']['name']))
    status_in_progress = session.query('Status where name="{}"'.format('In progress')).one()
    task['status'] = status_in_progress
    session.commit()


def tools_init(session):
    ''' Add our custom tool to the main ftrack menu '''

    ftrack_menu = get_ftrack_menu(submenu_name=None)

    cmds.menuItem(
        parent=ftrack_menu,
        label='Set in progress',
        command=(
            lambda x: set_in_progress(session)
        )
    )


def initialise():
    # TODO : later we need to bring back here all the maya initialiations
    #  from ftrack-connect-maya
    # such as frame start / end etc....

    logger.debug('Setting up the menu')
    session = ftrack_api.Session(auto_connect_event_hub=False)

    event_manager = event.QEventManager(
        session=session, mode=constants.LOCAL_EVENT_MODE
    )

    maya_host.MayaHost(event_manager)

    cmds.loadPlugin('ftrackMayaPlugin.py', quiet=True)

    from ftrack_connect_pipeline_maya.client import load
    from ftrack_connect_pipeline_maya.client import publish
    from ftrack_connect_pipeline_maya.client import asset_manager
    from ftrack_connect_pipeline_maya.client import log_viewer

    # Enable loader and publisher only if is set to run local (default)
    dialogs = []

    dialogs.append(
        (load.MayaLoaderClient, 'Loader')
    )
    dialogs.append(
        (publish.MayaPublisherClient, 'Publisher')
    )
    dialogs.append(
        (asset_manager.MayaAssetManagerClient, 'Asset Manager')
    )
    dialogs.append(
        (log_viewer.MayaLogViewerClient, 'Log Viewer')
    )

    ftrack_menu = get_ftrack_menu()
    # Register and hook the dialog in ftrack menu
    for item in dialogs:
        if item == 'divider':
            cmds.menuItem(divider=True)
            continue

        dialog_class, label = item

        cmds.menuItem(
            parent=ftrack_menu,
            label=label,
            command=(
                lambda x, dialog_class=dialog_class: _open_dialog(dialog_class, event_manager)
            )
        )

    scene_init(session)

    tools_init(session)

    timeline_init()



cmds.evalDeferred('initialise()', lp=True)
